package pl.pmajewski.timelogger.classic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimeloggerClassicApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimeloggerClassicApplication.class, args);
    }

}
