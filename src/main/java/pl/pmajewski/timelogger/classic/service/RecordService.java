package pl.pmajewski.timelogger.classic.service;

import pl.pmajewski.timelogger.classic.dto.RecordCreateDTO;
import pl.pmajewski.timelogger.classic.dto.RecordDTO;
import pl.pmajewski.timelogger.classic.dto.RecordUpdateDTO;

import java.time.LocalDate;
import java.util.List;

public interface RecordService {

    RecordDTO create(RecordCreateDTO body);

    RecordDTO delete(Long id);

    RecordDTO update(RecordUpdateDTO body);

    List<RecordDTO> all();
}
