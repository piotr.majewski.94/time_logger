package pl.pmajewski.timelogger.classic.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RecordCreateDTO {

    private String name;
    private LocalDateTime begin;
    private LocalDateTime end;
}
