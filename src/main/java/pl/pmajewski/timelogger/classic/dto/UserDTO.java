package pl.pmajewski.timelogger.classic.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserDTO {

    private Long id;
    private String email;
    private LocalDateTime registerDate;

    public UserDTO(Long id, String email, LocalDateTime registerDate) {
        this.id = id;
        this.email = email;
        this.registerDate = registerDate;
    }
}
